pragma solidity ^0.8.0;

import "./interfaces/ISablier.sol";

contract KeroWrapper {
    ISablier wrapped_sablier;

    constructor(address _sablier) {
        wrapped_sablier = ISablier(_sablier);
    }

    function withdrawFromStream(uint256 streamId, uint256 amount) 
        public
        returns (bool) {
        return wrapped_sablier.withdrawFromStream(streamId, amount);
    }
}
